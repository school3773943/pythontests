import allure
import pytest
from allure_commons.types import AttachmentType
from src.page_objects.page_object import KinopoiskPage


@pytest.mark.parametrize("film_name", ["Форрест"])
@pytest.mark.parametrize("country", ["США"])
@pytest.mark.parametrize("genre", ["драма"])
@pytest.mark.parametrize("full_film_name", ["Форрест Гамп"])
@allure.title("Test FindFilm")
def test_play(chrome, film_name, country, genre, full_film_name):
    page = KinopoiskPage(chrome)
    with allure.step("Открываем страницу расширеного поиска."):
        page.go_to_site()
        allure.attach(chrome.get_screenshot_as_png(), name="page_shot1", attachment_type=AttachmentType.PNG)
    with allure.step("В поле поиска пишем не полное название фильма."):
        page.find_film_field(film_name)
        allure.attach(chrome.get_screenshot_as_png(), name="page_shot2", attachment_type=AttachmentType.PNG)
    with allure.step("В блоке выбора страны, выбираем нужную."):
        page.country_select(country)
        allure.attach(chrome.get_screenshot_as_png(), name="page_shot3", attachment_type=AttachmentType.PNG)
    with allure.step("В блоке выбора жанра выбираем нужный."):
        page.genre_select(genre)
        allure.attach(chrome.get_screenshot_as_png(), name="page_shot4", attachment_type=AttachmentType.PNG)
    with allure.step("Нажимаем на кнопку ПОИСК в блоке ИСКАТЬ ФИЛЬМ."):
        page.find_button().click()
        allure.attach(chrome.get_screenshot_as_png(), name="page_shot5", attachment_type=AttachmentType.PNG)
    with allure.step("Проверяем что фильм находится в ТОП5 поисковой выдачи."):
        page.names_of_films(full_film_name)
        allure.attach(chrome.get_screenshot_as_png(), name="page_shot6", attachment_type=AttachmentType.PNG)
    with allure.step("Выводим основную информацию о фильме."):
        page.most_wanted_film()
        allure.attach(chrome.get_screenshot_as_png(), name="page_shot7", attachment_type=AttachmentType.PNG)

from src.base_pages.base_page import BasePage
from selenium.webdriver.common.by import By


class KinopoiskLocators:
    """Здесь хранится список локаторов страницы"""

    FIND_FILM = (By.XPATH, '//input[@id="find_film"]')
    COUNTRY_SELECT = (By.XPATH, '//select[@id="country"]')
    GENRE_SELECT = (By.XPATH, '//select[@id="m_act[genre]"]')
    FIND_BUTTON = (By.XPATH, '//input[@value="поиск"] ')
    FILM_NAMES = (By.XPATH, '//p[@class="name"]/a[@data-type="film"]')
    MOST_WANTED_FILM_INFO = (By.XPATH, '//div[@class="element most_wanted"]/div[@class="info"]')


class KinopoiskPage(BasePage):

    def find_film_field(self, film_name):
        find_film = self.find_element(KinopoiskLocators.FIND_FILM, time=10)
        find_film.clear()
        find_film.send_keys(film_name)
        return find_film

    def country_select(self, country_name):
        select = self.select_element(KinopoiskLocators.COUNTRY_SELECT, time=10)
        select.select_by_visible_text(country_name)
        return select

    def genre_select(self, genre_name):
        select = self.select_element(KinopoiskLocators.GENRE_SELECT, time=10)
        select.select_by_visible_text(genre_name)
        return select

    def find_button(self):
        return self.find_element(KinopoiskLocators.FIND_BUTTON, time=10)

    def names_of_films(self, full_film_name):
        names = self.find_elements(KinopoiskLocators.FILM_NAMES, time=10)
        movies = []
        for name in names:
            if len(movies) == 5:
                break
            else:
                movies.append(name.text)
        if full_film_name in movies:
            print("Искомый фильм в ТОП5 поисковой выдачи.")
        else:
            print("/nВ ТОП5 поисковой выдачи нет искомого фильма.")

    def most_wanted_film(self):
        info = self.find_element(KinopoiskLocators.MOST_WANTED_FILM_INFO, time=10)
        print(info.text)








